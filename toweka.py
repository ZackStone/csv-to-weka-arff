import os
import csv
import re
import xlrd

def retirarCharEspecial(w):
	w = w.replace(u'\ufeff', '')
	w = w.replace('\'', '')
	return w

def substituirVirgulaPorPonto(w):
	return w.replace(',', '.')

def substituirValoresNulos(w):
	valoresNulos = ['NULL', ' ']
	if w in valoresNulos:
		w = '?'
	return w

def colocarAspas(w):
	if w == '?':
		return w
	return '\'' + w + '\''

def ehData(w):
	#1991-12-16 00:00:00.0000000
	#1991-12-16 00:00:00.000
	regex = r'\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\.\d{1,7}'
	if re.search(regex, w):
		return True
	return False

def formatarData(w):
	return w[0:10]

def ajustarValor(w):
	try:
		return str(int(w))
	except ValueError:
		w = str(w)

	w = retirarCharEspecial(w)
	
	if ehData(w):
		w = formatarData(w)
	else:
		w = substituirVirgulaPorPonto(w)
		w = substituirValoresNulos(w)
		w = colocarAspas(w)
	return w

label_idade = ['16-21', '22-27', '28-33', '34-39', '40-45', '46-51', '52-57', '58-63', '64-69']
def clusterizar_idade(idade):
	try:
		idade = int(idade)
	except ValueError:
		return '?'

	if idade < 16 or idade >= 70:
		return '?'
	return label_idade[int((idade - 16) / 6)]

def read_csv(output_file):
	input_filepath = os.path.join('temp', 'input.csv')
	with open(input_filepath, newline='', encoding='utf-8') as csvfile:
		spamreader = csv.reader(csvfile, delimiter=';')
		for row in spamreader:
			newRow = []
			for col in row:
				valor = ajustarValor(str(col))
				newRow.append(valor)

			linha = ','.join(newRow)
			output_file.write(linha + '\n')


def read_xls():

	arquivos = dict()
	arquivos2 = dict()
	flag = dict()

	for i in range(1,10):
		output_filepath = os.path.join('temp', 'output-' + str(i) + '-train.arff')
		arquivos[i] = open(output_filepath, 'w')
		flag[i] = False

	for i in range(1,10):
		output_filepath = os.path.join('temp', 'output-' + str(i) + '-test.arff')
		arquivos2[i] = open(output_filepath, 'w')

	flag['null'] = False
	arquivos['null'] = open(os.path.join('temp', 'output-null-train.arff'), 'w')
	arquivos2['null'] = open(os.path.join('temp', 'output-null-test.arff'), 'w')

	head = '''@relation aecrh

@attribute Id_sexo {1, 2}
@attribute Id_grau_instrucao {20, 21, 22, 23, 24, 25, 26}
@attribute Id_estado_civil {1, 2, 3, 4, 5}
@attribute Id_jovem_aprendiz {0, 1}
@attribute Id_perfil_candidato {1, 2}
@attribute Id_tipo_divulgacao {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
@attribute Id_cidade {1, 2, 3, 4, 5, 6, 7, 8, 9}
@attribute idade {16-21, 22-27, 28-33, 34-39, 40-45, 46-51, 52-57, 58-63, 64-69}
@attribute ano numeric
@attribute mes {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}
@attribute class {0, 1}

@data
'''

	for a in arquivos:
		arquivos[a].write(head)
		arquivos2[a].write(head)


	input_filepath = os.path.join('temp', 'input.xlsx')
	workbook = xlrd.open_workbook(input_filepath)
	sheet = workbook.sheet_by_index(0)
	for row in range(1, sheet.nrows):
		values = []

		count_attr_nulos = 0

		for col in range(sheet.ncols):
			value = sheet.cell(row,col).value
			value = ajustarValor(value)

			if col == (sheet.ncols - 1): # ultima coluna, da classe
				if (str(value) == '2'):
					#value = 'aprovado'
					value = 1
				else:
					#value = 'reprovado'
					value = 0
			elif col == 7: # indice da coluna que tem a idade
				value = clusterizar_idade(value)

			if value == '?':
				count_attr_nulos += 1
			values.append(str(value))

		if count_attr_nulos <= 5:
			print(values)
			linha = ','.join(values)

			cidade = None
			try:
				cidade = int(sheet.cell(row, 6).value)
			except ValueError:
				cidade = 'null'

			if flag[cidade]:
				arquivos[cidade].write(linha + '\n')
				flag[cidade] = not flag[cidade]
			else:
				arquivos2[cidade].write(linha + '\n')
				flag[cidade] = not flag[cidade]

	for a in arquivos:
		arquivos[a].close()


def main():

	read_xls()
	return

	output_filepath = os.path.join('temp', 'output.txt')
	with open(output_filepath, 'w') as output_file:
		read_csv(outputFile)


main()
print('pronto!')